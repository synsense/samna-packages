This repository collects PyPi packages for samna.

### Install

Samna can be installed automatically with `pip`:

```
pip install samna --index-url https://gitlab.com/api/v4/projects/27423070/packages/pypi/simple
```

### Issues

Help us improve our product by opening an issue in the official tracker: https://gitlab.com/synsense/samna-issues/-/issues